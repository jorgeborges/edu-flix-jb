class Movie < ActiveRecord::Base
  RATINGS = %w(G PG PG-13 R NC-17)
  CULT_CLASSIC_REVIEW_SIZE = 50
  CULT_CLASSIC_REVIEW_AVERAGE_STARS = 4

  has_many :reviews, dependent: :destroy

  has_attached_file :image, styles: {
    small: "90x133>",
    thumb: "50x50>"
  }

  validates :title, :released_on, :duration, presence: true
  validates :description, length: {minimum: 25}
  validates :total_gross, numericality: {greater_than_or_equal_to: 0}
  validates :image_file_name, allow_blank: true, format: {
      with:    /\w+.(gif|jpg|png)\z/i,
      message: "must reference a GIF, JPG, or PNG image"
    }
  validates_attachment :image,
    :content_type => { :content_type => ['image/jpeg', 'image/png'] },
    :size => { :less_than => 1.megabyte }
  validates :rating, inclusion: {in: RATINGS}

  def flop?
    (total_gross.blank? || total_gross < 50000000) && !cult_classic?
  end

  def cult_classic?
    reviews.size >= CULT_CLASSIC_REVIEW_SIZE && average_stars >= CULT_CLASSIC_REVIEW_AVERAGE_STARS
  end

  def self.released
    where("released_on <= ?", Time.now).order("released_on desc")
  end

  def average_stars
    reviews.average :stars
  end

  def recent_reviews
    reviews.order("created_at desc").limit 2
  end
end
