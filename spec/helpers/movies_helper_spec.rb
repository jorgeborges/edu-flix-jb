require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the MoviesHelper. For example:
#
# describe MoviesHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
describe MoviesHelper do
  it "formats average stars with asterisks" do
    movie1 = Movie.create(movie_attributes(title: "Iron Man"))
    movie1.reviews.create(review_attributes(name: "Roger Ebert", stars: 5))
    movie1.reviews.create(review_attributes(name: "Gene Siskel", stars: 1))

    expect(helper.format_average_stars(movie1)).to eq("*** 3.0 stars")
  end
end
