require 'spec_helper'

describe "Deleting a review" do
  it "destroys the review and shows the movie listing without the deleted review" do
    movie1 = Movie.create(movie_attributes)
    review1 = movie1.reviews.create(review_attributes(name: "Roger Ebert"))
    review2 = movie1.reviews.create(review_attributes(name: "Gene Siskel"))

    visit movie_reviews_url(movie1)

    expect(page).to have_text(review1.name)
    expect(page).to have_text(review2.name)

    click_link "delete_review_#{review1.id}"

    expect(current_path).to eq(movie_reviews_path(movie1))
    expect(page).not_to have_text(review1.name)
    expect(page).to have_text(review2.name)
    expect(page).to have_text('Review successfully deleted!')
  end
end
