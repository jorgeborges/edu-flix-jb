require 'spec_helper'

describe "Viewing an individual movie" do
  it "shows the movie's details" do
    movie = Movie.create(movie_attributes(total_gross: 318412101))
    visit movie_url(movie)
    expect(page).to have_text(movie.title)
    expect(page).to have_text(movie.rating)
    expect(page).to have_text(movie.description)
    expect(page).to have_text(movie.released_on)
    expect(page).to have_text(movie.cast)
    expect(page).to have_text(movie.director)
    expect(page).to have_text(movie.duration)
    expect(page).to have_selector("img[src$='#{movie.image.url}']")
  end

  it "shows the total gross if the total gross exceeds $50M" do
    movie = Movie.create(movie_attributes(total_gross: 318412101))
    visit movie_url(movie)
    expect(page).to have_text("$318,412,101.00")
  end

  it "shows 'Flop!' if the total gross is less than $50M" do
    movie = Movie.create(movie_attributes(total_gross: 0))
    visit movie_url(movie)
    expect(page).to have_text("Flop!")
  end

  it "shows a placeholder image for a movie without poster" do
    movie = Movie.create(movie_attributes(image: nil))
    visit movie_url(movie)
    expect(page).to have_selector("img[src$='placeholder.png']")
  end
end
