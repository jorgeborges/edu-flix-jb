require 'spec_helper'

describe "Editing a review" do

  it "updates the review and shows the movie's reviews" do
    movie1 = Movie.create(movie_attributes)
    review1 = movie1.reviews.create(review_attributes(name: "Roger Ebert"))
    review2 = movie1.reviews.create(review_attributes(name: "Gene Siskel"))

    visit movie_reviews_url(movie1)

    expect(page).to have_text(review1.name)
    expect(page).to have_text(review2.name)

    click_link "edit_review_#{review1.id}"

    expect(current_path).to eq(edit_movie_review_path(movie1, review1))

    expect(find_field('Name').value).to eq(review1.name)

    fill_in 'Name', with: "John Doe"
    click_button 'Update Review'

    expect(current_path).to eq(movie_reviews_path(movie1))

    expect(page).to have_text("John Doe")
    expect(page).to have_text('Review successfully updated!')
  end

  it "does not update the review if it's invalid" do
    movie = Movie.create(movie_attributes)
    review = movie.reviews.create(review_attributes(name: "Roger Ebert"))

    visit edit_movie_review_url(movie, review)

    fill_in 'Name', with: " "

    click_button 'Update Review'

    expect(page).to have_text('error')
  end
end
