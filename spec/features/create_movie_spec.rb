require 'spec_helper'

describe "Creating a new movie" do
  it "saves the movie and shows the new movie's details" do
    visit movies_url

    click_link 'Add New Movie'

    expect(current_path).to eq(new_movie_path)

    fill_in "Title", with: "New Movie Title"
    fill_in "Description", with: "Superheroes saving the world from villains"
    select "PG-13", :from => "movie_rating"
    fill_in "Total gross", with: "75000000"
    fill_in "Released on", with: "2007/12/25"
    fill_in "Cast", with: "The award-winning cast"
    fill_in "Director", with: "The ever-creative director"
    fill_in "Duration", with: "123 min"
    attach_file "Image", "#{Rails.root}/app/assets/images/ironman.jpg"
    click_button 'Create Movie'

    movie = Movie.last
    expect(current_path).to eq(movie_path(movie))

    expect(page).to have_text('New Movie Title')
    expect(page).to have_text('Superheroes saving the world from villains')
    expect(page).to have_text('PG-13')
    expect(page).to have_text('$75,000,000.00')
    expect(page).to have_text('December 25, 2007')
    expect(page).to have_text('The award-winning cast')
    expect(page).to have_text('The ever-creative director')
    expect(page).to have_text('123 min')
    expect(page).to have_text('The ever-creative director')
    expect(page).to have_selector("img[src$='#{movie.image.url}']")
    expect(page).to have_text('Movie successfully created!')
  end

  it "does not save the movie if it's invalid" do
    visit new_movie_url

    expect {
      click_button 'Create Movie'
    }.not_to change(Movie, :count)

    expect(current_path).to eq(movies_path)
    expect(page).to have_text('error')
  end
end
